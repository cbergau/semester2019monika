package de.mycompany.todolistsemesterarbeit;

import android.view.View;
import android.widget.ImageView;

import de.mycompany.todolistsemesterarbeit.database.TaskRepository;
import de.mycompany.todolistsemesterarbeit.entities.Task;

public class ImageViewDoneClickListener implements View.OnClickListener {
    private ImageView imageView;
    private TaskRepository repository;
    private Task task;
    private TaskArrayAdapter taskArrayAdapter;

    ImageViewDoneClickListener(
            TaskArrayAdapter taskArrayAdapter,
            ImageView imageViewDone,
            Task task,
            TaskRepository repository
    ) {
        this.taskArrayAdapter = taskArrayAdapter;
        this.imageView = imageViewDone;
        this.task = task;
        this.repository = repository;
    }

    public void onClick(View v) {
        if (task.isOpen()) {
            task.close();

            if (repository.updateTask(task)) {
                imageView.setImageResource(R.drawable.no);
                taskArrayAdapter.notifyDataSetChanged();
            }
        } else {
            task.open();

            if (repository.updateTask(task)) {
                imageView.setImageResource(R.drawable.ok);
                taskArrayAdapter.notifyDataSetChanged();
            }
        }
    }
}
