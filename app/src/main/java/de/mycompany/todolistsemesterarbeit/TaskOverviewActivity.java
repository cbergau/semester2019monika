package de.mycompany.todolistsemesterarbeit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.util.ArrayList;

import de.mycompany.todolistsemesterarbeit.database.TaskRepository;
import de.mycompany.todolistsemesterarbeit.database.TaskSQLiteRepository;

/**
 * Übersicht aller Aufgaben
 */
public class TaskOverviewActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_EDIT_TASK = 95;
    public static final int REQUEST_CODE_NEW_TASK = 94;
    private ListView listViewTasks;
    private int taskId;
    private ArrayList id_list;
    private String orderby = "finished ASC";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_list);

        // Eine Sortierung wird ausgelesen und erstellt.
        Bundle bundle = getIntent().getExtras();

        if (bundle.getString("orderby").equals("priority ASC, date DESC")) {
            orderby = "priority ASC, date DESC";
        } else if (bundle.getString("orderby").equals("date DESC, priority ASC")) {
            orderby = "date DESC, priority ASC";
        } else {
            orderby = "finished ASC";
        }

        presentTasks();
    }

    private void presentTasks() {
        // Daten für die Listenübersicht werden ausgelesen.
        TaskRepository repository = new TaskSQLiteRepository(this);
        id_list = repository.getAllID(orderby);

        // Mit Hilfe des Adapters werden, die Daten dementsprechend angezeigt.
        TaskArrayAdapter adapter = new TaskArrayAdapter(this, repository.findAll(orderby), repository);

        listViewTasks = findViewById(R.id.listViewTasks);
        listViewTasks.setAdapter(adapter);
        listViewTasks.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                taskId = (int) id_list.get(arg2);
                Bundle dataBundle = new Bundle();
                dataBundle.putInt("taskId", taskId);
                Intent intent = new Intent(getApplicationContext(), TaskActivity.class);
                intent.putExtras(dataBundle);
                startActivityForResult(intent, REQUEST_CODE_EDIT_TASK);
            }
        });
    }

    //Die obere Menüleiste wird hinzugefügt.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_overview, menu);
        return true;
    }

    //Funktionen zum Menü werden hinzugefügt: Neues Note, Sortierung
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_add_task:
                Bundle dataBundle = new Bundle();
                dataBundle.putInt("taskId", 0);
                Intent intent = new Intent(getApplicationContext(), TaskActivity.class);
                intent.putExtras(dataBundle);
                startActivityForResult(intent, REQUEST_CODE_NEW_TASK);
                return true;
            case R.id.action_sortbyDown:
                orderby = TaskSQLiteRepository.COLUMN_FINISHED + " ASC, "
                        + TaskSQLiteRepository.COLUMN_DUE_DATE + " ASC";
                presentTasks();
                return true;
            case R.id.action_sortbyUp:
                orderby = TaskSQLiteRepository.COLUMN_IS_FAVOURITE + " ASC, "
                        + TaskSQLiteRepository.COLUMN_DUE_DATE + " ASC";
                presentTasks();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                presentTasks();
                break;
            case RESULT_CANCELED:
            default:
                break;
        }
    }
}
