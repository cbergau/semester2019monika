package de.mycompany.todolistsemesterarbeit;

import android.Manifest;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import de.mycompany.todolistsemesterarbeit.database.TaskSQLiteRepository;
import de.mycompany.todolistsemesterarbeit.entities.Contact;
import de.mycompany.todolistsemesterarbeit.entities.Task;

public class TaskActivity extends AppCompatActivity {
    private TaskSQLiteRepository mydb;
    public final int PICK_CONTACT = 1;
    private TextView name;
    private TextView description;
    private TextView dueDate;
    private TextView dueTime;
    private RadioButton radioPriorityNormal;
    private RadioButton radioPriorityHigh;
    private RadioGroup radioGroup;
    private String setprio;
    private String s;
    private TextView textViewDisplayName;
    private ImageView imageIsDone;
    private ImageView deleteCon;
    private ImageView sendSMS;
    private ImageView sendMail;
    private TextView textViewPhoneNumber;
    private TextView textViewEmail;
    private int taskId = 0;
    private String phonenumber;

    public static final int SELECT_PHONE_NUMBER = 1;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_edit);
        name = findViewById(R.id.editTextNote);
        description = findViewById(R.id.editTextDescription);
        dueDate = findViewById(R.id.editTextDate);
        dueTime = findViewById(R.id.editTextTime);
        textViewDisplayName = findViewById(R.id.ContactView);
        radioPriorityNormal = findViewById(R.id.radioButtonNormal);
        radioPriorityHigh = findViewById(R.id.radioButtonHoch);
        radioGroup = findViewById(R.id.radioGroup);
        imageIsDone = findViewById(R.id.Image_Finished);
        deleteCon = findViewById(R.id.deleteContact);
        sendSMS = findViewById(R.id.btn_sendSMS);
        sendMail = findViewById(R.id.btn_sendMail);
        textViewEmail = findViewById(R.id.textViewMail);
        textViewPhoneNumber = findViewById(R.id.textViewSMS);
        mydb = new TaskSQLiteRepository(this);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int todoId = extras.getInt("taskId");

            if (todoId > 0) {
                final Task todo = mydb.getById(todoId);
                this.taskId = todoId;
                presentTask(todo);
            } else {
                presentTask(new Task());
            }
        }
    }

    private void presentTask(final Task task) {
        if (task.getId() > 0) {
            if (task.isDone()) {
                imageIsDone.setImageResource(R.drawable.ok);
            } else {
                imageIsDone.setImageResource(R.drawable.no);
            }

            imageIsDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (task.isOpen()) {
                        task.close();
                        imageIsDone.setImageResource(R.drawable.ok);
                        mydb.updateTask(task);
                    } else {
                        task.open();
                        imageIsDone.setImageResource(R.drawable.no);
                        mydb.updateTask(task);
                    }

                    setResult(RESULT_OK);
                    finish();
                }
            });

            name.setText(task.getName());
            description.setText(task.getDescription());
        } else {
            imageIsDone.setVisibility(View.INVISIBLE);
            imageIsDone.setFocusable(View.NOT_FOCUSABLE);
        }

        if (task.hasDueDate()) {
            dueDate.setText(task.getDueDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
            dueTime.setText(task.getDueDate().format(DateTimeFormatter.ofPattern("HH:mm")));
        }

        if (task.isFavourite()) {
            radioPriorityHigh.setChecked(true);
        } else {
            radioPriorityNormal.setChecked(true);
        }

        if (task.hasContact()) {
            presentContact(task.getContact());
        }

        if (textViewPhoneNumber.getText().equals("")) {
            sendSMS.setVisibility(View.INVISIBLE);
            sendSMS.setEnabled(false);
        } else {
            sendSMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendSMSMessage();
                }
            });
        }

        if (hasInternetConnection()) {
            if (textViewEmail.getText().equals("")) {
                sendMail.setVisibility(View.INVISIBLE);
                sendMail.setEnabled(false);
            } else {
                sendMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendEmail();
                    }
                });
            }
        } else {
            sendMail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "Es besteht keine Internetverbindung.", Toast.LENGTH_SHORT).show();
                }
            });
        }

        dueDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DateDialog dialog = new DateDialog(v);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "DatePicker");
            }
        });

        dueTime.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TimeDialog dialog = new TimeDialog(v);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "TimePicker");
            }
        });

        textViewDisplayName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PERMISSIONS_REQUEST_READ_CONTACTS
                );
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            if (extras.getInt("taskId") > 0) {
                getMenuInflater().inflate(R.menu.menu_task, menu);
            }
        }

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.deleteTask:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.deleteTaskConfirmation)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mydb.deleteTask(taskId);
                                Toast.makeText(getApplicationContext(), "Erfolgreich gelöscht.", Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // Falls Nutzer das Löschen abgebrochen hat.
                            }
                        });
                AlertDialog d = builder.create();
                d.setTitle("Sind Sie sicher?");
                d.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Email-Sende-Funktion. Alle notwendigen Angaben werden ausgelesen
    protected void sendEmail() {
        Log.i("Send email", "");
        String[] TO = {textViewEmail.getText().toString()};


        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Notiz: " + name.getText());
        emailIntent.putExtra(Intent.EXTRA_TEXT, description.getText());

        try {
            startActivity(Intent.createChooser(emailIntent, "Sende Email..."));
            finish();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "Es wurde kein Email-Client installiert.", Toast.LENGTH_SHORT).show();
        }
    }

    //SMS-Sende-Funktion. Alle notwendigen Angaben werden ausgelesen
    protected void sendSMSMessage() {
        String phoneNo = textViewPhoneNumber.getText().toString();
        String message = name.getText() + " - " + description.getText();

        if (phonenumber != "") {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, message, null, null);
                Toast.makeText(getApplicationContext(), "SMS gesendet.", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "SMS konnte nicht gesendet werden.", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Keine Telefonnummber vorhanden.", Toast.LENGTH_LONG).show();
        }

    }

    //Überprüfung, ob mit Internet verbunden.
    private boolean hasInternetConnection() {
        boolean Wifi = false;
        boolean Mobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo NI : netInfo) {
            if (NI.getTypeName().equalsIgnoreCase("WIFI")) {
                if (NI.isConnected()) {
                    Wifi = true;
                }
            }
            if (NI.getTypeName().equalsIgnoreCase("MOBILE"))
                if (NI.isConnected()) {
                    Mobile = true;
                }
        }
        return Wifi || Mobile;
    }


    //Kontaktliste wird aufgerufen, zur Auswahl eines Kontaktes aus dem Telefonbuch
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_CONTACT && resultCode == RESULT_OK) {
            Uri contactUri = data.getData();
            Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);
            cursor.moveToFirst();
            int columnIndexNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int columnIndexDisplayName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

            ContentResolver contentResolver = getContentResolver();
            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            String email = "";

            Cursor cursorEmail = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                    new String[]{contactId},
                    null);

            if (cursorEmail.moveToFirst()) {
                email = cursorEmail.getString(
                        cursorEmail.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)
                );
            }

            Contact contact = new Contact(
                    cursor.getString(columnIndexDisplayName),
                    cursor.getString(columnIndexNumber),
                    email
            );

            presentContact(contact);
        }
    }

    private void presentContact(Contact contact) {
        textViewPhoneNumber.setText(contact.getPhoneNumber());
        textViewEmail.setText(contact.getEmail());
        textViewDisplayName.setText(contact.getName());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(i, SELECT_PHONE_NUMBER);
            } else {
                Toast.makeText(this, "Keine Erlaubnis Kontakte zu lesen.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void run(View view) {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int todoId = extras.getInt("taskId");

            if (todoId > 0) {
                Task existingTask = mydb.getById(todoId);
                mapViewToTask(existingTask);

                if (mydb.updateTask(existingTask)) {
                    Toast.makeText(getApplicationContext(), "Aufgabe wurde aktualisiert", Toast.LENGTH_SHORT).show();

                    // Diese Ansicht schließen
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Aufgabe konnte nicht aktualisiert werden", Toast.LENGTH_SHORT).show();
                }
            } else {
                Task newTask = new Task();
                mapViewToTask(newTask);

                if (mydb.createNewTask(newTask)) {
                    Toast.makeText(getApplicationContext(), "Aufgabe wurde erfolgreich erstellt", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Aufgabe konnte nicht erstellt werden", Toast.LENGTH_SHORT).show();
                }

                setResult(RESULT_OK);
                finish();
            }
        }
    }

    private void mapViewToTask(Task todo) {
        todo.setName(name.getText().toString());
        todo.setDescription(description.getText().toString());
        todo.setFavourite(radioPriorityHigh.isChecked());
        todo.setContact(new Contact(
                textViewDisplayName.getText().toString(),
                textViewPhoneNumber.getText().toString(),
                textViewEmail.getText().toString())
        );

        // Wenn kein Datum eingegeben wurde, wird "heute" genommen
        String actualDate = dueDate.getText().toString().equals("")
                ? LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
                : dueDate.getText().toString();

        // Wenn keine Zeit eingegeben wurde, wird 00:00 Uhr genommen
        String actualTime = dueTime.getText().toString().equals("")
                ? "00:00"
                : dueTime.getText().toString();

        todo.setDueDate(LocalDateTime.parse(
                actualDate + " " + actualTime,
                DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")
        ));
    }
}
