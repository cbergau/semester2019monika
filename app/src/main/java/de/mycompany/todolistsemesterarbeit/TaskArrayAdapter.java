package de.mycompany.todolistsemesterarbeit;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.time.format.DateTimeFormatter;
import java.util.List;

import de.mycompany.todolistsemesterarbeit.ImageViewDoneClickListener;
import de.mycompany.todolistsemesterarbeit.ImportanceViewClickListener;
import de.mycompany.todolistsemesterarbeit.R;
import de.mycompany.todolistsemesterarbeit.database.TaskRepository;
import de.mycompany.todolistsemesterarbeit.entities.Task;

public class TaskArrayAdapter extends ArrayAdapter<Task> {
    private final Context context;
    private List<Task> tasks;
    private TaskRepository repository;

    public TaskArrayAdapter(Context context, List<Task> tasks, TaskRepository repository) {
        super(context, R.layout.task_row, tasks);
        this.tasks = tasks;
        this.context = context;
        this.repository = repository;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.task_row, parent, false);

        TextView textViewNote = rowView.findViewById(R.id.note);
        TextView textViewDueDate = rowView.findViewById(R.id.date);
        ImageView imageViewDone = rowView.findViewById(R.id.finished);
        ImageView imageViewFavourite = rowView.findViewById(R.id.showImportance);

        Task task = tasks.get(position);
        textViewNote.setText(task.getName());
        textViewDueDate.setText(task.getDueDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")));

        // Offene Tasks werden mit einem offenen Kreis gekenntzeichnet
        if (task.isOpen()) {
            imageViewDone.setImageResource(R.drawable.no);
        } else {
            imageViewDone.setImageResource(R.drawable.ok);
        }

        // Favoriten werden mit einem ausgefüllten Stern gekenntzeichnet
        if (task.isFavourite()) {
            imageViewFavourite.setImageResource(R.drawable.high);
        } else {
            imageViewFavourite.setImageResource(R.drawable.normal);
        }

        // Fertige Tasks werden durchgestrichen
        if (task.isDone()) {
            textViewNote.setPaintFlags(textViewNote.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        // Abgelaufene Tasks bekommen rote Icons
        if (task.isOverdue()) {
            if (task.isFavourite()) {
                imageViewFavourite.setImageResource(R.drawable.redstar);
            } else {
                imageViewFavourite.setImageResource(R.drawable.normal);
            }

            imageViewDone.setImageResource(R.drawable.red);
            textViewDueDate.setTextColor(Color.RED);
            textViewNote.setTextColor(Color.RED);
        }

        // Tasks auf erledigt setzen (Klick auf das Bild)
        imageViewDone.setOnClickListener(
                new ImageViewDoneClickListener(this, imageViewDone, task, repository)
        );

        imageViewFavourite.setOnClickListener(
                new ImportanceViewClickListener(this, imageViewFavourite, task, repository)
        );

        return rowView;
    }
}
