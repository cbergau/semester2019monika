package de.mycompany.todolistsemesterarbeit.entities;

import java.time.LocalDateTime;

public class Task {
    private int id;
    private String name;
    private String description;
    private boolean isFavourite;
    private boolean isDone;
    private LocalDateTime dueDate;
    private Contact contact;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public boolean isOpen() {
        return !isDone;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public boolean hasDueDate() {
        return dueDate != null;
    }

    public boolean isOverdue() {
        if (isDone()) {
            return false;
        }

        if (!hasDueDate()) {
            return false;
        }

        return LocalDateTime.now().isAfter(dueDate);
    }

    public void open() {
        isDone = false;
    }

    public void close() {
        isDone = true;
    }

    public void unfavourite() {
        isFavourite = false;
    }

    public void favourite() {
        isFavourite = true;
    }

    public boolean hasContact() {
        return contact != null;
    }
}
