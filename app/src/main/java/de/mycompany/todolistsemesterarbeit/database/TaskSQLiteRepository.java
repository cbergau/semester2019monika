package de.mycompany.todolistsemesterarbeit.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import de.mycompany.todolistsemesterarbeit.entities.Contact;
import de.mycompany.todolistsemesterarbeit.entities.Task;

public class TaskSQLiteRepository extends SQLiteOpenHelper implements TaskRepository {
    private static final String DATABASE_NAME = "tasks.db";
    private static final String TABLE_TASK = "tasks";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NOTE = "note";
    public static final String COLUMN_DUE_DATE = "due_date";
    private static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_IS_FAVOURITE = "is_favourite";
    public static final String COLUMN_FINISHED = "finished";
    private static final String COLUMN_CONTACT_NAME = "contact";
    private static final String COLUMN_CONTACT_MAIL = "mail";
    private static final String COLUMN_CONTACT_PHONE = "phone";
    private static final String DATE_FORMAT = "dd-MM-yyyy HH:mm";

    public TaskSQLiteRepository(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + TABLE_TASK + " (" +
                        COLUMN_ID + " integer PRIMARY KEY, " +
                        COLUMN_NOTE + " text," +
                        COLUMN_DESCRIPTION + " text, " +
                        COLUMN_DUE_DATE + " text, " +
                        COLUMN_IS_FAVOURITE + " text, " +
                        COLUMN_FINISHED + " text, " +
                        COLUMN_CONTACT_NAME + " text, " +
                        COLUMN_CONTACT_PHONE + " text, " +
                        COLUMN_CONTACT_MAIL + " text" +
                        ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASK);
        onCreate(db);
    }

    /**
     * Schreiben eines neuen Tasks die SQLite Datenbank
     *
     * @param task Das neu zu erstellenden Task
     * @return Gibt true zurück, wenn der Task erfolgreich erstellt wurde
     */
    @Override
    public boolean createNewTask(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NOTE, task.getName());
        contentValues.put(COLUMN_DESCRIPTION, task.getDescription());
        contentValues.put(COLUMN_DUE_DATE, task.getDueDate().format(DateTimeFormatter.ofPattern(DATE_FORMAT)));
        contentValues.put(COLUMN_IS_FAVOURITE, task.isFavourite() ? "1" : "0");
        contentValues.put(COLUMN_FINISHED, task.isDone() ? "1" : "0");

        if (task.hasContact()) {
            contentValues.put(COLUMN_CONTACT_NAME, task.getContact().getName());
            contentValues.put(COLUMN_CONTACT_PHONE, task.getContact().getPhoneNumber());
            contentValues.put(COLUMN_CONTACT_MAIL, task.getContact().getEmail());
        }

        long lastId = db.insert(TABLE_TASK, null, contentValues);
        task.setId((int) lastId);
        return lastId > -1;
    }

    /**
     * Task anhand einer ID ermitteln
     *
     * @param id Task ID
     * @return Gibt den Task aus der Datenbank zurück
     */
    @Override
    public Task getById(int id) {
        Task todo = new Task();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_TASK + " WHERE id = " + id, null);
        cursor.moveToFirst();

        mapResultSet(id, todo, cursor);

        cursor.close();
        db.close();

        return todo;
    }

    private void mapResultSet(int id, Task todo, Cursor cursor) {
        todo.setId(id);
        todo.setName(cursor.getString(cursor.getColumnIndex(TaskSQLiteRepository.COLUMN_NOTE)));
        todo.setDescription(cursor.getString(cursor.getColumnIndex(TaskSQLiteRepository.COLUMN_DESCRIPTION)));
        todo.setDone(cursor.getString(cursor.getColumnIndex(TaskSQLiteRepository.COLUMN_FINISHED)).equals("1"));
        todo.setDueDate(LocalDateTime.parse(
                cursor.getString(cursor.getColumnIndex(TaskSQLiteRepository.COLUMN_DUE_DATE)),
                DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")
        ));
        Contact contact = new Contact(
                cursor.getString(cursor.getColumnIndex(TaskSQLiteRepository.COLUMN_CONTACT_NAME)),
                cursor.getString(cursor.getColumnIndex(TaskSQLiteRepository.COLUMN_CONTACT_PHONE)),
                cursor.getString(cursor.getColumnIndex(TaskSQLiteRepository.COLUMN_CONTACT_MAIL))
        );
        todo.setContact(contact);
        todo.setFavourite(cursor.getString(cursor.getColumnIndex(TaskSQLiteRepository.COLUMN_IS_FAVOURITE)).equals("1"));
        todo.setDone(cursor.getString(cursor.getColumnIndex(TaskSQLiteRepository.COLUMN_FINISHED)).equals("1"));
    }

    @Override
    public boolean updateTask(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NOTE, task.getName());
        contentValues.put(COLUMN_DESCRIPTION, task.getDescription());
        contentValues.put(COLUMN_DUE_DATE, task.getDueDate().format(DateTimeFormatter.ofPattern(DATE_FORMAT)));
        contentValues.put(COLUMN_IS_FAVOURITE, task.isFavourite() ? "1" : "0");
        contentValues.put(COLUMN_FINISHED, task.isDone() ? "1" : "0");

        if (task.hasContact()) {
            contentValues.put(COLUMN_CONTACT_PHONE, task.getContact().getPhoneNumber());
            contentValues.put(COLUMN_CONTACT_MAIL, task.getContact().getEmail());
            contentValues.put(COLUMN_CONTACT_NAME, task.getContact().getName());
        }

        db.update(TABLE_TASK, contentValues, COLUMN_ID + " = ? ", new String[]{Integer.toString(task.getId())});

        return true;
    }

    /**
     * Ein Task auf erledigt setzen.
     *
     * @param task Task
     * @return true, wenn der Task erfolgreich auf erledigt gesetzt wurde
     */
    @Override
    public boolean closeTask(Task task) {
        return updateFinished(task, "1");
    }

    /**
     * Ein Task auf nicht erledigt setzen.
     *
     * @param task Task
     * @return true, wenn der Task erfolgreich auf nicht erledigt gesetzt wurde
     */
    @Override
    public boolean openTask(Task task) {
        return updateFinished(task, "0");
    }

    @Override
    public boolean updateFinished(Task todo, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_FINISHED, value);
        return db.update(TABLE_TASK, contentValues, COLUMN_ID + " = ? ", new String[]{Integer.toString(todo.getId())}) == 1;
    }

    /**
     * Einen Task als Favoriten Task markieren.
     *
     * @param taskId Task ID
     * @return true, wenn der Task erfolgreich als Favorit markiert wurde
     */
    @Override
    public boolean markFavourite(int taskId) {
        return updateFavourite(getById(taskId), "1");
    }

    /**
     * Einen Task als nicht-Favoriten Task markieren.
     *
     * @param taskId Task ID
     * @return true, wenn der Task erfolgreich als nicht-Favorit markiert wurde
     */
    @Override
    public boolean unmarkFavourite(int taskId) {
        return updateFavourite(getById(taskId), "0");
    }

    @Override
    public boolean updateFavourite(Task todo, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IS_FAVOURITE, value);
        return db.update(TABLE_TASK, contentValues, COLUMN_ID + " = ? ", new String[]{Integer.toString(todo.getId())}) == 1;
    }

    /**
     * Löschen eines Tasks
     *
     * @param id Task ID
     * @return true, wenn der Task gelöscht wurde
     */
    @Override
    public Integer deleteTask(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int delete = db.delete(TABLE_TASK,
                COLUMN_ID + " = ?",
                new String[]{Integer.toString(id)});
        return delete;
    }

    //Alle Note-Namen werden ausgelesen.
    @Override
    public ArrayList<String> getAllNotes(String orderby) {
        ArrayList<String> array_list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_TASK + " ORDER BY " + orderby, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            array_list.add(res.getString(res.getColumnIndex(COLUMN_NOTE)));
            res.moveToNext();
        }

        return array_list;
    }

    //Alle Note-Wichtigkeiten werden ausgelesen.
    @Override
    public ArrayList<String> getAllPriority(String orderby) {
        ArrayList<String> array_list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_TASK + " ORDER BY " + orderby, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            array_list.add(res.getString(res.getColumnIndex(COLUMN_IS_FAVOURITE)));
            res.moveToNext();
        }

        return array_list;
    }

    //Alle Note-Datum(s) werden ausgelesen.
    @Override
    public ArrayList<String> getAllDate(String orderby) {
        ArrayList<String> array_list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_TASK + " ORDER BY " + orderby, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex(COLUMN_DUE_DATE)));
            res.moveToNext();
        }

        return array_list;
    }

    //Alle Note-Erledigt werden ausgelesen.
    @Override
    public ArrayList<String> getAllFinished(String orderby) {
        ArrayList<String> array_list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_TASK + " ORDER BY " + orderby, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            array_list.add(res.getString(res.getColumnIndex(COLUMN_FINISHED)));
            res.moveToNext();
        }

        return array_list;
    }

    //Alle Note-IDs werden ausgelesen.
    @Override
    public ArrayList<Integer> getAllID(String orderby) {
        ArrayList<Integer> array_list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_TASK + " ORDER BY " + orderby, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }

        return array_list;
    }

    @Override
    public List<Task> findAll(String orderby) {
        List<Task> tasks = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_TASK + " ORDER BY " + orderby, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            Task task = new Task();
            int id = Integer.valueOf(res.getString(res.getColumnIndex(TaskSQLiteRepository.COLUMN_ID)));
            mapResultSet(id, task, res);
            tasks.add(task);
            res.moveToNext();
        }

        res.close();

        return tasks;
    }
}
