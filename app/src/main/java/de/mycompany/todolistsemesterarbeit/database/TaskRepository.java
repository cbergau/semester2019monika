package de.mycompany.todolistsemesterarbeit.database;

import java.util.ArrayList;
import java.util.List;

import de.mycompany.todolistsemesterarbeit.entities.Task;

public interface TaskRepository {
    /**
     * Schreiben eines neuen Tasks die SQLite Datenbank
     *
     * @param task Das neu zu erstellenden Task
     * @return Gibt true zurück, wenn der Task erfolgreich erstellt wurde
     */
    boolean createNewTask(Task task);

    /**
     * Task anhand einer ID ermitteln
     *
     * @param id Task ID
     * @return Gibt den Task aus der Datenbank zurück
     */
    Task getById(int id);

    boolean updateTask(Task todo);

    /**
     * Ein Task auf erledigt setzen.
     *
     * @param task Task
     * @return true, wenn der Task erfolgreich auf erledigt gesetzt wurde
     */
    boolean closeTask(Task task);

    /**
     * Ein Task auf nicht erledigt setzen.
     *
     * @param task Task
     * @return true, wenn der Task erfolgreich auf nicht erledigt gesetzt wurde
     */
    boolean openTask(Task task);

    boolean updateFinished(Task todo, String value);

    /**
     * Einen Task als Favoriten Task markieren.
     *
     * @param taskId Task ID
     * @return true, wenn der Task erfolgreich als Favorit markiert wurde
     */
    boolean markFavourite(int taskId);

    /**
     * Einen Task als nicht-Favoriten Task markieren.
     *
     * @param taskId Task ID
     * @return true, wenn der Task erfolgreich als nicht-Favorit markiert wurde
     */
    boolean unmarkFavourite(int taskId);

    boolean updateFavourite(Task todo, String value);

    /**
     * Löschen eines Tasks
     *
     * @param id Task ID
     * @return true, wenn der Task gelöscht wurde
     */
    Integer deleteTask(int id);

    //Alle Note-Namen werden ausgelesen.
    ArrayList<String> getAllNotes(String orderby);

    //Alle Note-Wichtigkeiten werden ausgelesen.
    ArrayList<String> getAllPriority(String orderby);

    //Alle Note-Datum(s) werden ausgelesen.
    ArrayList<String> getAllDate(String orderby);

    //Alle Note-Erledigt werden ausgelesen.
    ArrayList<String> getAllFinished(String orderby);

    //Alle Note-IDs werden ausgelesen.
    ArrayList<Integer> getAllID(String orderby);

    List<Task> findAll(String orderby);
}
