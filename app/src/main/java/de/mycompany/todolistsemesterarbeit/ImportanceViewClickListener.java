package de.mycompany.todolistsemesterarbeit;

import android.view.View;
import android.widget.ImageView;

import de.mycompany.todolistsemesterarbeit.database.TaskRepository;
import de.mycompany.todolistsemesterarbeit.entities.Task;

public class ImportanceViewClickListener implements View.OnClickListener {
    private ImageView imageView;
    private TaskRepository repository;
    private Task task;
    private TaskArrayAdapter taskArrayAdapter;

    ImportanceViewClickListener(
            TaskArrayAdapter taskArrayAdapter,
            ImageView imageViewDone,
            Task task,
            TaskRepository repository
    ) {
        this.taskArrayAdapter = taskArrayAdapter;
        this.imageView = imageViewDone;
        this.task = task;
        this.repository = repository;
    }

    public void onClick(View v) {
        if (task.isFavourite()) {
            task.unfavourite();
            imageView.setImageResource(R.drawable.normal);

            if (repository.updateTask(task)) {
                taskArrayAdapter.notifyDataSetChanged();
            }
        } else {
            task.favourite();
            imageView.setImageResource(R.drawable.high);

            if (repository.updateTask(task)) {
                taskArrayAdapter.notifyDataSetChanged();
            }
        }
    }
}
