Installation
===

Projekt klonen
===

Terminal öffnen

    cd ~/AndroidProjects
    git clone https://gitlab.com/cbergau/semester2019monika
    cd semester2019monika

Projekt starten
===

 * Android Studio öffnen
 * Open existing Project
 * ~/AndroidProjects/semster2019monika
 * Gradle baut im Hintergrund, sieht man unten in der Leiste
 * Alle Emulatoren löschen
 * Neuen Emulator mit Android SDK 28 anlegen
 * Mit dem Emulator bei Google anmelden für Kontake
 * App starten

Lokales Git Repository löschen
===

Terminal öffnen

    cd ~/AndroidProjects/semester2019monika
    rm -rf .git
